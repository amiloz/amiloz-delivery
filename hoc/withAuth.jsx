/* eslint-disable react/display-name */
import { useRouter } from 'next/router'
import { logoutUser } from '../store/slices/authSlice'
import { useDispatch } from 'react-redux'

const withAuth = (WrappedComponent) => {
  return (props) => {
    const dispatch = useDispatch()
    // checks whether we are on client / browser or server.
    if (typeof window !== 'undefined') {
      const Router = useRouter()
      console.log('rout', Router.pathname)

      const accessToken = JSON.parse(
        localStorage.getItem('token')
      )?.access_token
      const expiresIn = JSON.parse(localStorage.getItem('token'))?.expires_in
      const expiresInDate = expiresIn ? new Date(expiresIn) : ''
      const nowDate = new Date()

      // If there is no access token we redirect to "/login" page.
      if (!accessToken) {
        dispatch(logoutUser())
        return null
      }
      if (expiresInDate < nowDate) {
        dispatch(logoutUser())
        return null
      }

      // If this is an accessToken we just render the component that was passed with all its props

      return <WrappedComponent {...props} />
    }

    // If we are on server, return null
    return null
  }
}

export default withAuth
