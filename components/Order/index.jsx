import axios from 'axios'
import { useEffect, useRef, useState } from 'react'
import { Col, Image, Row } from 'react-bootstrap'
import styles from './index.module.css'
import moment from 'moment'
import OrderDeleteModal from './OrderDeleteModal'
import OrderEditModal from './OrderEditModal'
import DatePicker from 'react-datepicker'
import { toast } from 'react-toastify'
import ViewSummaryModal from './ViewSummaryModal'
import DropdownTask from './DropdownTask'
import { useRouter } from 'next/dist/client/router'
import OrderDeliveryModal from './OrderDeliveryModal'
import Pagination from 'react-js-pagination'
import withAuth from '../../hoc/withAuth'

const Orders = () => {
  const [showEditModal, setShowEditModal] = useState(false)
  const [showDeleteModal, setShowDeleteModal] = useState(false)
  const [showSummaryModal, setShowSummaryModal] = useState(false)
  const [showDeliveryModal, setShowDeliveryModal] = useState(false)
  const [industries, setIndustries] = useState([])
  const [selectedName, setSelectedName] = useState({})
  const [sortDirection, setSortDirection] = useState(false)
  const [displaySortMenu, setDisplaySortMenu] = useState(false)
  const [filterKey, setFilterKey] = useState(null)
  const [sortKey, setSortKey] = useState(null)
  const [date, setDate] = useState('')
  const [stores, setStores] = useState([])
  const [storeId, setStoreId] = useState('')
  const [orderType, setOrderType] = useState('today')
  const [selectedOrders, setSelectedOrders] = useState([])
  const [selectedOrdersData, setSelectedOrdersData] = useState([])
  const [selectAllOrders, setSelectAllOrders] = useState(false)
  const [productsSummary, setProductsSummary] = useState([])
  const [windowWidth, setWindowWidth] = useState(0)
  const [paidAmount, setPaidAmount] = useState('')
  const [loading, setLoading] = useState(false)
  const [assignLoader, setAssignLoader] = useState(false)
  const [activePage, setActivePage] = useState(1)
  const [total, setTotal] = useState(0)
  const router = useRouter()

  useEffect(() => {
    setWindowWidth(window.innerWidth)
  }, [])

  function useOutsideAlerterSort(ref) {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setDisplaySortMenu(false)
        }
      }

      // Bind the event listener
      document.addEventListener('mousedown', handleClickOutside)
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener('mousedown', handleClickOutside)
      }
    }, [ref])
  }

  const sortRef = useRef(null)
  useOutsideAlerterSort(sortRef)

  const getAllIndusries = (
    dateValue,
    page = 1,
    filterKeyValue,
    type = 'today',
    sortKeyValue
  ) => {
    const token = JSON.parse(localStorage.getItem('token'))
    let url = `/delivery-reps/orders?type=${type}&_limit=30&_page=${page}`
    if (dateValue) {
      url += `&date=${moment(dateValue).format('YYYY-MM-DD')}`
    }
    if (filterKeyValue) {
      url += `&store_id=${filterKeyValue}`
    }
    if (sortKeyValue) {
      const dir = sortDirection === true ? 'desc' : 'asc'
      url += `&sort_by=${sortKeyValue}&sort_dir=${dir}`
    }
    axios({
      method: 'GET',
      url,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => {
      setIndustries(res.data.orders)
      setTotal(res.data.total)
    })
  }
  const getAllStores = () => {
    const token = JSON.parse(localStorage.getItem('token'))
    axios({
      method: 'GET',
      url: `/stores`,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => setStores(res.data.stores))
  }
  useEffect(() => {
    getAllIndusries()
    getAllStores()
  }, [])
  const sortKeyChangeHandler = (e) => {
    const { value } = e.target
    if (e.target.checked) {
      setSortKey(value)
      getAllIndusries(null, 1, filterKey, value)
    } else {
      setSortKey(null)
      getAllIndusries(null, 1, filterKey, null)
    }
    setDisplaySortMenu(false)
  }
  const selectedOrdersChangeHandler = (id, industry, e) => {
    if (e.target.checked) {
      const selectedArr = [...selectedOrders.filter((com) => com !== ''), id]
      const selectedArrData = [
        ...selectedOrdersData.filter((com) => com !== ''),
        industry,
      ]
      setSelectedOrders(selectedArr)
      setSelectedOrdersData(selectedArrData)
    } else {
      const filterArr = selectedOrders.filter((pc) => pc !== id)
      const filterArrData = selectedOrdersData.filter((pc) => pc.id !== id)
      setSelectedOrders(filterArr)
      setSelectedOrdersData(filterArrData)
    }
  }
  const allOrdersChangeHandler = (e) => {
    if (e.target.checked) {
      const allOrders = industries?.map((ind) => ind.id)
      setSelectedOrders(allOrders)
      setSelectAllOrders(true)
      setSelectedOrdersData(industries)
    } else {
      setSelectedOrders([])
      setSelectedOrdersData([])
      setSelectAllOrders(false)
    }
  }
  const assignPickupOrder = (e) => {
    e.preventDefault()
    setAssignLoader(true)
    axios({
      method: 'POST',
      url: `/delivery-reps/orders/pick-up`,
      data: {
        order_ids: selectedOrders,
      },
    })
      .then((res) => {
        setShowSummaryModal(false)
        setAssignLoader(false)
        setSelectedOrders([])
        setSelectAllOrders(false)
        toast('Picked up marked!')
        getAllIndusries(date, 1, storeId, orderType)
      })
      .catch((err) => {
        setAssignLoader(false)
        toast('Error marking picked up')
      })
  }
  const deliverOrder = (id) => {
    setLoading(true)
    axios({
      method: 'POST',
      url: `/delivery-reps/orders/${id}/deliver`,
      data: {
        paid_amount: parseInt(paidAmount),
      },
    })
      .then((res) => {
        toast('Order delivered marked!')
        getAllIndusries(date, 1, storeId, orderType)
        setShowDeliveryModal(false)
        setLoading(false)
      })
      .catch((err) => {
        setLoading(false)
        toast('Error marking delivered')
      })
  }
  const viewSummary = () => {
    setShowSummaryModal(true)
    axios({
      method: 'POST',
      url: `/delivery-reps/orders/summary`,
      data: {
        order_ids: selectedOrders,
      },
    }).then((res) => {
      setProductsSummary(res.data.products)
    })
  }
  return (
    <div className={styles.Container}>
      <Row>
        <Col lg={6} md={12}>
          <div style={{ position: 'relative' }}>
            <button
              onClick={() => viewSummary(true)}
              type="button"
              className={styles.myCartButton}
              disabled={selectedOrders.length === 0}
              style={
                selectedOrders.length === 0
                  ? { background: '#e7e7e7', cursor: 'not-allowed' }
                  : {}
              }
            >
              Asignar repartidor
            </button>
          </div>
        </Col>
        <Col>
          <div className={styles.filterSortDiv}>
            <div style={{ marginRight: '10px' }}>
              <DatePicker
                selected={date}
                onChange={(date) => {
                  setDate(date)
                  getAllIndusries(date, 1, storeId)
                }}
                placeholderText="Filtrar por fecha"
                className={styles.modalInputDate}
              />
            </div>
            <div className={styles.miniDiv}>
              <div>
                {/* <label>Filter by Store</label> */}
                <select
                  value={storeId}
                  onChange={(e) => {
                    setStoreId(e.target.value)
                    getAllIndusries(date, 1, e.target.value)
                  }}
                  className={styles.modalInput}
                  style={{
                    height: '32px',
                    paddingTop: '0',
                    paddingBottom: '0',
                  }}
                >
                  <option value="">All</option>
                  {stores?.map((store) => (
                    <option key={store.id} value={store.id}>
                      {store.name}
                    </option>
                  ))}
                </select>
              </div>
              <div className={styles.sortByDiv}>
                <span onClick={() => setDisplaySortMenu(!displaySortMenu)}>
                  Ordenar por{' '}
                  <span style={{ float: 'right' }}>
                    <i className="fa fa-caret-down" aria-hidden="true"></i>
                  </span>
                </span>
                {displaySortMenu ? (
                  <ul ref={sortRef}>
                    <li
                      className={sortKey === 'name' && styles.sortByDivLi}
                      style={{ padding: '12px' }}
                    >
                      <label>
                        <span
                          style={{ border: 'none', background: 'transparent' }}
                        >
                          <input
                            type="checkbox"
                            value="name"
                            checked={sortKey === 'name'}
                            onChange={(e) => sortKeyChangeHandler(e)}
                            style={{ display: 'none' }}
                          />
                        </span>
                        Category Name
                      </label>
                    </li>
                  </ul>
                ) : null}
              </div>
            </div>
          </div>
        </Col>
      </Row>
      <div className={styles.Tab}>
        <span
          className={
            orderType === 'today' ? styles.TabActive : styles.TabInactive
          }
          onClick={() => {
            setOrderType('today')
            getAllIndusries(date, 1, storeId, 'today')
          }}
        >
          Entregas de hoy
        </span>
        <span
          style={{ marginLeft: '41px' }}
          className={
            orderType === 'upcoming' ? styles.TabActive : styles.TabInactive
          }
          onClick={() => {
            setOrderType('upcoming')
            getAllIndusries(date, 1, storeId, 'upcoming')
          }}
        >
          Próximas entregas
        </span>
      </div>
      {windowWidth > 800 ? (
        industries.length === 0 ? (
          <h4>No hay pedidos para mostrar</h4>
        ) : (
          <>
            <table className={styles.myTable}>
              <thead>
                <tr>
                  <th className={styles.theadTh1}>
                    <input
                      type="checkbox"
                      onChange={(e) => allOrdersChangeHandler(e)}
                      checked={selectAllOrders}
                    />
                  </th>
                  <th className={styles.theadTh1}>Fecha</th>
                  <th className={styles.theadTh1}>Fecha estimada de entrega</th>
                  <th className={styles.theadTh}>Tiendas</th>
                  <th className={styles.theadTh1}>Total</th>
                  <th className={styles.theadTh1}>Estatus</th>
                  <th className={styles.theadTh1}>Forma de pago</th>
                  <th className={styles.theadTh1}>Ver detalles</th>
                  <th className={styles.theadTh1}>Entregar</th>
                  <th className={styles.theadTh1}>Acción</th>
                </tr>
              </thead>
              <tbody>
                {industries?.map((industry) => {
                  const {
                    id,
                    date,
                    grand_total,
                    status,
                    store,
                    created_at,
                    payment_method,
                  } = industry
                  return (
                    <tr
                      key={id}
                      className={styles.tbodyTr}
                      style={{ margin: '12px 0' }}
                    >
                      <td className={styles.tbodyTd1}>
                        <input
                          type="checkbox"
                          onChange={(e) =>
                            selectedOrdersChangeHandler(id, industry, e)
                          }
                          checked={selectedOrders.includes(id)}
                        />
                      </td>
                      <td className={styles.tbodyTd1}>
                        {moment(created_at).format('LL')}
                      </td>
                      <td className={styles.tbodyTd1}>
                        {moment(date).format('LL')}
                      </td>
                      <td className={styles.tbodyTd}>
                        <div>
                          <span className={styles.OrderName}>{store.name}</span>
                          <span className={styles.OrderContact}>
                            {store.mobile}
                          </span>
                          <br />
                          <span className={styles.OrderName}>
                            {store.poc_name}
                          </span>
                          <span className={styles.OrderContact}>
                            {store.poc_mobile}
                          </span>
                          <br />
                          <span className={styles.OrderContact}>
                            {store.street}, {store.city}, {store.state},{' '}
                            {store.country}
                          </span>
                        </div>
                      </td>
                      <td className={styles.tbodyTd1}>{grand_total}</td>
                      <td className={styles.tbodyTd1}>{status}</td>
                      <td className={styles.tbodyTd1}>{payment_method}</td>
                      <td className={styles.tbodyTd1}>
                        <button
                          onClick={() => {
                            router.push(`/order/${id}`)
                          }}
                          className={styles.buttonEditProd}
                        >
                          Ver detalles
                        </button>
                      </td>
                      <td className={styles.tbodyTd1}>
                        <button
                          type="button"
                          onClick={() => {
                            setSelectedName(industry)
                            setShowDeliveryModal(true)
                          }}
                          className={styles.buttonEditProd}
                          disabled={status !== 'Picked Up'}
                          style={
                            status !== 'Picked Up'
                              ? { background: '#e7e7e7' }
                              : {}
                          }
                        >
                          Entregar pedido
                        </button>
                      </td>
                      <td className={styles.tbodyTd1}>
                        <button
                          onClick={() => {
                            setShowEditModal(true)
                            setSelectedName(industry)
                          }}
                          disabled={status !== 'Picked Up'}
                          className={styles.buttonEditProd}
                          style={
                            status !== 'Picked Up'
                              ? { background: '#e7e7e7', cursor: 'not-allowed' }
                              : {}
                          }
                        >
                          Editar
                        </button>
                        <button
                          onClick={() => {
                            setShowDeleteModal(true)
                            setSelectedName(industry)
                          }}
                          className={styles.buttonDeleteProd}
                        >
                          Cancelar
                        </button>
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </>
        )
      ) : (
        <>
          <div>
            {industries?.map((industry) => {
              const { id, date, grand_total, status, payment_method, store } =
                industry
              return (
                <div key={id} className={styles.tableMobDiv}>
                  <div className={styles.tableMobDivFlex}>
                    <input
                      type="checkbox"
                      onChange={(e) =>
                        selectedOrdersChangeHandler(id, industry, e)
                      }
                      checked={selectedOrders.includes(id)}
                      style={{ marginRight: '10px' }}
                    />
                    <span className={styles.tableMobDivFlex1}>
                      {moment(date).format('LL')}
                    </span>
                    <span className={styles.tableMobDivFlex2}>
                      <DropdownTask
                        openEditModal={() => {
                          setShowEditModal(true)
                          setSelectedName(industry)
                        }}
                        openDeleteModal={() => {
                          setShowDeleteModal(true)
                          setSelectedName(industry)
                        }}
                        selectedOrder={industry}
                      />
                    </span>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <span>Tiendas: </span>
                    <br />
                    <div className={styles.tableMobDivDownInfo}>
                      <div>
                        <span className={styles.OrderName}>{store.name}</span>
                        <span className={styles.OrderContact}>
                          {store.mobile}
                        </span>
                        <br />
                        <span className={styles.OrderName}>
                          {store.poc_name}
                        </span>
                        <span className={styles.OrderContact}>
                          {store.poc_mobile}
                        </span>
                        <br />
                        <span className={styles.OrderContact}>
                          {store.street}, {store.city}, {store.state},{' '}
                          {store.country}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <span>Total: </span>
                    <br />
                    <div className={styles.tableMobDivDownInfo}>
                      {grand_total}
                    </div>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <span>Estatus: </span>
                    <div className={styles.tableMobDivDownInfo}>{status}</div>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <span>Forma de pago: </span>
                    <div className={styles.tableMobDivDownInfo}>
                      {payment_method}
                    </div>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <div className={styles.tableMobDivDownInfo}>
                      <button
                        type="button"
                        onClick={() => {
                          setSelectedName(industry)
                          setShowDeliveryModal(true)
                        }}
                        className={styles.buttonEditProd}
                        disabled={status !== 'Picked Up'}
                        style={
                          status !== 'Picked Up'
                            ? { background: '#e7e7e7' }
                            : {}
                        }
                      >
                        Entregar pedido
                      </button>
                    </div>
                  </div>
                  <div className={styles.tableMobDivDown}>
                    <div className={styles.tableMobDivDownInfo}>
                      <button
                        onClick={() => {
                          setShowDeleteModal(true)
                          setSelectedName(industry)
                        }}
                        className={styles.buttonDeleteProd}
                      >
                        Cancelar
                      </button>
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
        </>
      )}
      <div
        style={{
          marginTop: '20px',
          borderTop: '1px solid #e7e7e7',
          textAlign: 'right',
          paddingTop: '20px',
          minHeight: '62px',
        }}
      >
        <div style={{ float: 'right' }}>
          <Pagination
            activePage={activePage}
            itemsCountPerPage={30}
            totalItemsCount={total}
            pageRangeDisplayed={5}
            onChange={(page) => {
              setActivePage(page)
              getAllIndusries(date, page, storeId, orderType)
            }}
            itemClass="page-item"
            linkClass="page-link"
          />
        </div>
      </div>
      <OrderEditModal
        show={showEditModal}
        onHide={() => setShowEditModal(false)}
        handleClose={() => setShowEditModal(false)}
        getAllIndusries={getAllIndusries}
        selectedIndustry={selectedName}
        stores={stores}
      />
      <OrderDeleteModal
        show={showDeleteModal}
        onHide={() => setShowDeleteModal(false)}
        handleClose={() => setShowDeleteModal(false)}
        getAllIndusries={getAllIndusries}
        selectedIndustry={selectedName}
      />
      <ViewSummaryModal
        show={showSummaryModal}
        onHide={() => setShowSummaryModal(false)}
        handleClose={() => setShowSummaryModal(false)}
        productsSummary={productsSummary}
        assignPickupOrder={assignPickupOrder}
        selectedOrdersData={selectedOrdersData}
        assignLoader={assignLoader}
      />
      <OrderDeliveryModal
        show={showDeliveryModal}
        onHide={() => setShowDeliveryModal(false)}
        handleClose={() => setShowDeliveryModal(false)}
        selectedIndustry={selectedName}
        deliverOrder={deliverOrder}
        setPaidAmount={setPaidAmount}
        loading={loading}
      />
    </div>
  )
}

export default withAuth(Orders)
