import axios from 'axios'
import { useState } from 'react'
import { Form, Image, Modal } from 'react-bootstrap'
import styles from './index.module.css'

const OrderDeliveryModal = ({
  show,
  handleClose,
  deliverOrder,
  selectedIndustry,
  setPaidAmount,
  loading,
}) => {
  return (
    <Modal
      show={show}
      onHide={handleClose}
      fullscreen="md-down"
      centered={true}
      dialogClassName={styles.industryModal}
      style={{ background: 'rgba(0, 22, 38, 0.6)' }}
    >
      <Modal.Body className={styles.modalBodyDesktop}>
        <div className={styles.mobileModalDiv} onClick={() => handleClose()}>
          <p className={styles.mobileModalDivH}>
            <Image
              src="/images/arrowBackMobile.png"
              alt=""
              style={{ transform: 'translateY(-1px)', marginRight: '30px' }}
            />{' '}
            Deliver Order
          </p>
        </div>
        <h3 className={styles.modalHeadingMain}>Deliver Order</h3>
        <Form>
          <label className={styles.modalLabel}>Payment Method</label>
          <p className={styles.modalInfo}>{selectedIndustry.payment_method}</p>
          <label className={styles.modalLabel}>Grand Total</label>
          <p className={styles.modalInfo}>{selectedIndustry.grand_total}</p>
          <label className={styles.modalLabel}>Enter Amount</label>
          <input
            className={styles.modalInput}
            onChange={(e) => setPaidAmount(e.target.value)}
            style={{ display: 'block', width: '90%' }}
            type="text"
          />
          <div className={styles.modalButtonDiv}>
            <button
              type="button"
              disabled={loading}
              className={
                loading ? styles.modalDisabledButton : styles.modalSaveButton
              }
              onClick={() => deliverOrder(selectedIndustry.id)}
            >
              Confirm
            </button>
          </div>
        </Form>
      </Modal.Body>
    </Modal>
  )
}

export default OrderDeliveryModal
