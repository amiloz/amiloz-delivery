import axios from 'axios'
import { useEffect, useState } from 'react'
import { Col, Form, Image, Modal, Row } from 'react-bootstrap'
import styles from './index.module.css'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import { toast } from 'react-toastify'

const OrderEditModal = ({
  show,
  handleClose,
  getAllIndusries,
  selectedIndustry,
  stores,
}) => {
  const [loading, setLoading] = useState(false)
  const [products, setProducts] = useState([])

  useEffect(() => {
    const productsArr = selectedIndustry?.products
    setProducts(productsArr)
  }, [selectedIndustry])
  const removeProduct = (id) => {
    const prodArr = products?.filter((prod) => prod.id !== id)
    setProducts(prodArr)
  }
  const changeQuantity = (prodId, operation) => {
    let productQuantity = 0
    let productId = ''
    const prodCheck = products?.map((prod) => {
      if (prodId === prod.id) {
        prod.quantity =
          operation === 'add' ? prod.quantity + 1 : prod.quantity - 1
        productQuantity =
          operation === 'add' ? prod.quantity + 1 : prod.quantity - 1
        productId = prod.product_id
      }
      return prod
    })
    setProducts(prodCheck)
    quantitySubmitHandler(productId, productQuantity)
  }
  const quantitySubmitHandler = (productId, quantity) => {
    axios({
      method: 'PUT',
      url: `/delivery-reps/orders/${selectedIndustry.id}/products/${productId}`,
      data: {
        quantity,
      },
    }).then((res) => {
      toast('Order Edited!')
      getAllIndusries()
    })
  }
  return (
    <Modal
      show={show}
      onHide={handleClose}
      fullscreen="md-down"
      centered={true}
      dialogClassName={styles.industryModal}
      style={{ background: 'rgba(0, 22, 38, 0.6)' }}
    >
      <Modal.Body className={styles.modalBodyDesktop}>
        <div className={styles.mobileModalDiv} onClick={() => handleClose()}>
          <p className={styles.mobileModalDivH}>
            <Image
              src="/images/arrowBackMobile.png"
              alt=""
              style={{ transform: 'translateY(-1px)', marginRight: '30px' }}
            />{' '}
            Edit Order
          </p>
        </div>
        <h3 className={styles.modalHeadingMain}>Edit Order</h3>
        <Form>
          <Form.Label className={styles.modalLabel}>Products</Form.Label>
          <Row>
            {products?.map((prod) => {
              const { id, name, quantity } = prod
              return (
                <Col key={id} lg={4} style={{ marginBottom: '15px' }}>
                  <div className={styles.productItem}>
                    <div>
                      <h3 className={styles.OrderName}>{name}</h3>
                      <div className={styles.quantityDiv}>
                        <button
                          onClick={() => changeQuantity(id, 'minus')}
                          disabled={quantity === 1}
                          className={styles.quantityButton}
                          type="button"
                        >
                          -
                        </button>
                        <span>{quantity}</span>
                        <button
                          onClick={() => changeQuantity(id, 'add')}
                          className={styles.quantityButton}
                          type="button"
                        >
                          +
                        </button>
                      </div>
                    </div>
                  </div>
                </Col>
              )
            })}
          </Row>
          {/* <div className={styles.modalButtonDiv}>
            <button
              type="submit"
              className={
                loading ? styles.modalDisabledButton : styles.modalSaveButton
              }
              disabled={loading}
            >
              Save
            </button>
          </div> */}
        </Form>
      </Modal.Body>
    </Modal>
  )
}

export default OrderEditModal
