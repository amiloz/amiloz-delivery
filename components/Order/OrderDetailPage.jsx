import { useRouter } from 'next/dist/client/router'
import styles from './index.module.css'
import axios from 'axios'
import { useState, useEffect } from 'react'
import moment from 'moment'
import { Col, Row, Image } from 'react-bootstrap'
import withAuth from '../../hoc/withAuth'

function OrderDetailPage() {
  const router = useRouter()
  const [order, setOrder] = useState({})
  const orderId = router.query.orderId

  const getOrder = () => {
    const token = JSON.parse(localStorage.getItem('token'))
    axios({
      method: 'GET',
      url: `/delivery-reps/orders/${orderId}`,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => setOrder(res.data.order))
  }
  useEffect(() => {
    getOrder()
  }, [])
  console.log(order)
  return (
    <div className={styles.Container}>
      <button
        className={styles.redirectButton}
        onClick={() => router.push('/order')}
      >
        Regresar a lista pedidos
      </button>
      <h2 className={styles.orderPlaceDivHeading}>Resumen del pedido</h2>
      <Row>
        <Col lg={6} md={12}>
          <div className={styles.orderPlaceDiv}>
            <Row>
              <Col>
                <h3 className={styles.detailPageHeadingSub}>Fecha</h3>
                <span className={styles.OrderName}>
                  {order && moment(order.date).format('LL')}
                </span>
              </Col>
              <Col>
                <h3 className={styles.detailPageHeadingSub}>Estatus</h3>
                <span className={styles.OrderName}>
                  {order && order.status}
                </span>
              </Col>
              <Col>
                <h3 className={styles.detailPageHeadingSub}>Total</h3>
                <span className={styles.OrderName}>
                  {order && order.grand_total}
                </span>
              </Col>
            </Row>
            <Row
              style={{
                marginTop: '20px',
                borderTop: '1px solid #6f87a0',
                paddingTop: '15px',
              }}
            >
              <Col>
                <h3 className={styles.detailPageHeadingSub}>Vendedor</h3>
                <span className={styles.OrderName}>
                  {order && order.sales_rep_name}
                </span>
              </Col>
              <Col>
                <h3 className={styles.detailPageHeadingSub}>Repartidor</h3>
                <span className={styles.OrderName}>
                  {order && order.delivery_rep_name}
                </span>
              </Col>
            </Row>
          </div>
        </Col>
        <Col lg={6} md={12}>
          <div className={styles.orderPlaceDiv} style={{ height: '91%' }}>
            <h3 className={styles.detailPageHeadingSub}>
              Detalles de la tienda
            </h3>
            {order.store && (
              <div>
                <span className={styles.OrderName}>{order.store.name}</span>
                <span className={styles.OrderContact}>
                  {order.store.mobile}
                </span>
                <br />
                <span className={styles.OrderName}>{order.store.poc_name}</span>
                <span className={styles.OrderContact}>
                  {order.store.poc_mobile}
                </span>
                <br />
                <span className={styles.OrderContact}>
                  {order.store.street}, {order.store.city}, {order.store.state},{' '}
                  {order.store.country}
                </span>
              </div>
            )}
          </div>
        </Col>
      </Row>
      {order && (
        <div className={styles.orderPlaceDiv}>
          <Row>
            <h3 className={styles.detailPageHeadingSub}>Productos</h3>
            {order?.products?.map((prod) => {
              const {
                id,
                name,
                quantity,
                primary_image,
                brand_name,
                category_name,
                unit_in_package,
                price,
              } = prod
              return (
                <Col key={id} lg={4} style={{ marginBottom: '15px' }}>
                  <div className={styles.productItem}>
                    <div style={{ marginRight: '20px' }}>
                      <Image
                        src={primary_image}
                        alt=""
                        width="120px"
                        height="120px"
                      />
                    </div>
                    <div>
                      <div style={{ width: '100%' }}>
                        <h3
                          className={styles.productItemName}
                          style={{ display: 'inline-block' }}
                        >
                          {name}
                        </h3>
                      </div>
                      <h3 className={styles.OrderBrand}>{brand_name}</h3>
                      <h3 className={styles.OrderCategory}>{category_name}</h3>
                      <div className={styles.OrderQuantity}>
                        <span>
                          x {quantity} {unit_in_package}
                        </span>
                      </div>
                      <div className={styles.OrderPrice}>${price}</div>
                    </div>
                  </div>
                </Col>
              )
            })}
          </Row>
        </div>
      )}
    </div>
  )
}

export default withAuth(OrderDetailPage)
