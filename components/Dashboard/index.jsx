import styles from '../Order/index.module.css'
import { useSelector } from 'react-redux'
import { useEffect, useState } from 'react'
import { Col, Image, Row } from 'react-bootstrap'
import moment from 'moment'
import axios from 'axios'
import { useRouter } from 'next/dist/client/router'
import withAuth from '../../hoc/withAuth'
import DatePicker from 'react-datepicker'

const Dashboard = () => {
  const [industries, setIndustries] = useState([])
  const [summary, setSummary] = useState([])
  const [date, setDate] = useState(new Date())
  const [windowWidth, setWindowWidth] = useState(0)
  const { name } = useSelector((state) => state.auth)
  const router = useRouter()
  useEffect(() => {
    setWindowWidth(window.innerWidth)
  }, [])
  const getAllIndusries = (dateFilter = new Date()) => {
    const token = JSON.parse(localStorage.getItem('token'))
    let url = `/delivery-reps/orders?_limit=30&_page=1&date=${moment(
      dateFilter
    ).format('YYYY-MM-DD')}`
    axios({
      method: 'GET',
      url,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
    }).then((res) => {
      setIndustries(res.data.orders)
    })
  }
  const getSummary = (dateFilter = new Date()) => {
    const token = JSON.parse(localStorage.getItem('token'))
    axios({
      method: 'POST',
      url: `/delivery-reps/orders/summary`,
      headers: {
        Authorization: 'Bearer ' + token.access_token,
      },
      data: {
        date: moment(dateFilter).format('YYYY-MM-DD'),
      },
    }).then((res) => {
      setSummary(res.data.products)
    })
  }
  useEffect(() => {
    getAllIndusries()
    getSummary()
  }, [])
  return (
    <div className={styles.Container}>
      <h2 className={styles.orderPlaceDivHeading}>Bienvenido {name}</h2>
      <div>
        <label className={styles.modalLabel}>Filtrar por fecha</label>
        <DatePicker
          selected={date}
          onChange={(date) => {
            setDate(date)
            getAllIndusries(date)
            getSummary(date)
          }}
          placeholderText="Filtrar por fecha"
          className={styles.modalInputDate}
        />
      </div>
      <Row style={{ margin: '20px 0' }}>
        <Col
          lg={6}
          md={12}
          style={windowWidth > 1000 ? { paddingLeft: '0' } : {}}
        >
          <div className={styles.orderPlaceDiv}>
            <h3 className={styles.detailPageHeadingSub}>Pedidos</h3>
            <div
              style={{
                height: '64vh',
                overflowY: 'auto',
                overflowX: 'hidden',
                paddingRight: '7px',
              }}
            >
              {industries.length === 0 ? (
                <span className={styles.OrderName}>
                  No hay pedidos para mostrar
                </span>
              ) : (
                <table className={styles.myTable}>
                  <thead>
                    <tr>
                      <th className={styles.theadTh}>Tiendas</th>
                      <th className={styles.theadTh1}>Total</th>
                      <th className={styles.theadTh1}>Estatus</th>
                      <th className={styles.theadTh1}>Ver detalles</th>
                    </tr>
                  </thead>
                  <tbody>
                    {industries?.map((industry) => {
                      const { id, grand_total, status, store } = industry
                      return (
                        <tr
                          key={id}
                          className={styles.tbodyTr}
                          style={{ margin: '12px 0' }}
                        >
                          <td className={styles.tbodyTd}>
                            <div>
                              <span className={styles.OrderName}>
                                {store.name}
                              </span>
                              <span className={styles.OrderContact}>
                                {store.mobile}
                              </span>
                              <br />
                              <span className={styles.OrderName}>
                                {store.poc_name}
                              </span>
                              <span className={styles.OrderContact}>
                                {store.poc_mobile}
                              </span>
                              <br />
                              <span className={styles.OrderContact}>
                                {store.street}, {store.city}, {store.state},{' '}
                                {store.country}
                              </span>
                            </div>
                          </td>
                          <td className={styles.tbodyTd1}>{grand_total}</td>
                          <td className={styles.tbodyTd1}>{status}</td>
                          <td className={styles.tbodyTd1}>
                            <button
                              onClick={() => {
                                router.push(`/order/${id}`)
                              }}
                              className={styles.buttonEditProd}
                            >
                              Ver detalles
                            </button>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>
                </table>
              )}
            </div>
          </div>
        </Col>
        <Col lg={6} md={12}>
          <div className={styles.orderPlaceDiv}>
            <h3 className={styles.detailPageHeadingSub}>Resumen del pedido</h3>
            <div
              style={{
                height: '64vh',
                overflowY: 'auto',
                overflowX: 'hidden',
                paddingRight: '7px',
              }}
            >
              <Row>
                <Col lg={2} className={styles.orderSummaryHeader}>
                  Imagen
                </Col>
                <Col lg={5} className={styles.orderSummaryHeader}>
                  Nombre
                </Col>
                <Col lg={3} className={styles.orderSummaryHeader}>
                  Especificaciones
                </Col>
                <Col lg={2} className={styles.orderSummaryHeader}>
                  <div style={{ float: 'right' }}>Leftovers</div>
                </Col>
              </Row>
              {summary?.map((prod) =>
                summary.length === 0 ? (
                  <span className={styles.OrderName}>No orders today</span>
                ) : (
                  <div className={styles.summaryDiv} key={prod.product_id}>
                    <Row>
                      <Col lg={2}>
                        <Image
                          src={prod.primary_image}
                          alt=""
                          width="50px"
                          height="50px"
                        />
                      </Col>
                      <Col lg={5} style={{ paddingTop: '12px' }}>
                        <h2 className={styles.summaryHeading}>
                          {prod.product_name}
                        </h2>
                      </Col>
                      <Col lg={3} style={{ paddingTop: '12px' }}>
                        <p className={styles.summaryDesc}>
                          <span
                            style={{
                              fontSize: '12px',
                              lineHeight: '16px',
                              color: '#0F1111',
                              fontWeight: '500',
                            }}
                          >
                            Cantidad:
                          </span>{' '}
                          {prod.quantity}
                          <br />
                          <span
                            style={{
                              fontSize: '12px',
                              lineHeight: '16px',
                              color: '#0F1111',
                              fontWeight: '500',
                            }}
                          >
                            Peso:
                          </span>{' '}
                          {prod.weight} {prod.weight_unit}
                          <br />
                          <span
                            style={{
                              fontSize: '12px',
                              lineHeight: '16px',
                              color: '#0F1111',
                              fontWeight: '500',
                            }}
                          >
                            Unidad:
                          </span>{' '}
                          {prod.unit_in_package}
                        </p>
                      </Col>
                      <Col lg={2} style={{ paddingTop: '12px' }}>
                        <p
                          className={styles.summaryDesc}
                          style={{ float: 'right' }}
                        >
                          {prod.left_quantity}
                        </p>
                      </Col>
                    </Row>
                  </div>
                )
              )}
            </div>
          </div>
        </Col>
      </Row>
    </div>
  )
}

export default withAuth(Dashboard)
